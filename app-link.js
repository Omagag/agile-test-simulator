import { LitElement, html, css } from "lit-element";
import { navigator } from "lit-element-router";

export class Link extends navigator(LitElement) {
  static get properties() {
    return {
      href: { type: String }
    };
  }
  static get styles() {
    return css`
      li {
        float: left;
      }
      li>a {
        background-color:rgba(7, 33, 70);
        width: 150px;
        display: block;
        color: white;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
      }
      li>a:hover {
        color: black;
        background-color: RGBA(234,249,250);
      }
    `;
  }
  constructor() {
    super();
    this.href = "";
  }
  render() {
    return html`
      <li>
        <a href="${this.href}" @click="${this.linkClick}">
          <slot></slot>
        </a>
      </li>
    `;
  }
  linkClick(event) {
    event.preventDefault();
    this.navigate(this.href);
  }
}

customElements.define("app-link", Link);
