import { LitElement, html, css } from 'lit-element';
import '@material/mwc-formfield'
import '@material/mwc-radio'
import '@material/mwc-button'
import '@material/mwc-dialog'

class AppQuiz extends LitElement {

    static get styles() {
        return css `
      .quiz-container {
        width: 100%;  
        display: flex;
        justify-content: center;
      }
      .quiz-content {
        width: 70%;
        padding: 5px;
      }
      .quiz-content h2 {
          width: 100%;
          text-align: center;
          color: #072146;
      }
      .quiz-content p {
          color: #072146;
          font-weight: bold;
      }
      mwc-button{
        --mdc-theme-primary: navy;
        --mdc-theme-on-primary: white;
      }
      .errorContainer {
          width: 100%;
          height: 100px;
          display: flex;
          justify-content: center;
          align-items: center;
      }
      .errorMsg {
          color: #B92A45;
          font-size: 18px;
          font-weight: bold;
      }
    `;
    }

    static get properties() {
        return {
            preguntas: { type: Object },
            errorMsg: {type: String}
        };
    }

    constructor() {
        super();
        this.preguntas = {preguntas: []};
        this.errorMsg = "";
    }

    render() {
            return html `
      ${this.errorMsg !== "" ? html`
      <section class="errorContainer">
        <p class="errorMsg">${this.errorMsg}</p>
      </section>
      ` : html``}
      <article class="quiz-container">
          
      <div class="quiz-content">
          <h2>${this.preguntas.nombreExamen}</h2>
          ${this.preguntas.preguntas.map(p => html `
          <p>${p.pregunta}</p>
          ${p.respuestas.map(r => html`
            <mwc-formfield label=${r}>
                <mwc-radio name="location" @checked=${this.validarRespuesta}></mwc-radio>
            </mwc-formfield>
            <br/>
          `)}
          `)}
        <br/>
      <mwc-button raised label="Enviar" ></mwc-button>
      </div>
      </article>
    `;
    }
    connectedCallback() {// Sucede cuando termina la carga del componente en su contenedor donde se incluyo; en este caso index.html
        super.connectedCallback();
        try {
            this.loadData();
        } catch (e) {
            // alert(e);
            this.errorMsg = e;
        }
      }
    loadData() {
        fetch("http://localhost:8080/examen/")
        .then(response => {
            console.log(response);
            if (!response.ok) { throw response; }
            return response.json();
        })
        .then(data => {
            console.log(data);
            this.errorMsg = "";
            this.preguntas = data;
        })
        .catch(error => {
            //alert("Problemas con el fetch: " + error);
            this.errorMsg = "Hubo un conflicto al solicitar el recurso";
        });
      }
    validarRespuesta() {

    }
    validacionEmpty() {
        alert("Envia mensaje");
        return html `
            <mwc-dialog open>
                <div>Estas Seguro?</div>
            <mwc-button
                slot="primaryAction"
                dialogAction="discard">
                Aceptar
            </mwc-button>
            <mwc-button
                slot="secondaryAction"
                dialogAction="cancel">
                Cancelar
                    </mwc-button>
            </mwc-dialog>
        `

    }

    montarResultados() {
        this.campos = this.getObjProps(this.preguntas.respuestas);
        this.filas = [];
        for (var i = 0; i < this.preguntas.respuestas.length; i++) {
            this.resultados.respuestas[i].valores = this.getValores(this.preguntas.respuestas[i]);
            this.filas.push(i);

        }
        this.sig = (this.resultados.next !== null);
        this.prev = (this.resultados.previous !== null);
    }
    getValores(fila) {
        var valores = [];
        for (var i = 0; i < this.campos.length; i++) {
            valores[i] = fila[this.campos[i]];
        }
        return valores;
    }
}

customElements.define('app-quiz', AppQuiz);