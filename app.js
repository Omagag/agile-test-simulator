import { LitElement, html, css } from 'lit-element';
import {router} from 'lit-element-router';

import "./app-link";
import "./app-main";

import "./src/home-component";
import "./app-quiz";
import "./src/historia-component";

class App extends router(LitElement) {
  static get styles() {
    return css`
      :host {
        display: block;
      }
      #page-container {
        position: relative;
        min-height: 100vh;
      }
      .content {
        padding-bottom: 46px;
      }
      header{
        font-family: Helvetica;
        width: 100%;
        margin: 0 auto;
      }
      nav {
        width: 100%;
      }
      ul {
        list-style-type: none;
        width: 100%;
        margin: 0;
        padding: 0;
        overflow: hidden;
        background-color:rgba(7, 33, 70);
      }
      #menu>li {
        float: left;
        text-align:center
      }
      footer {
        width: 100%;
        bottom: 0;
        position: absolute;
      }
      footer section {
        width: 100;
        height: 46px;
        background-color:rgba(7, 33, 70);
        display: flex;
        justify-content: flex-end;
        align-items: center;
      }
      footer section p {
        color: #ccc;
        font-size: 12px;
        padding: 0 10px 0 10px;
      }
    `;
  }
  static get properties() {
    return {
      route: { type: String },
      params: { type: Object },
      query: { type: Object },
      data: { type: Object }
    };
  }

  static get routes() {
    return [
      {
        name: "home",
        pattern: "",
        data: { title: "Home" }
      },
      {
        name: "info",
        pattern: "info",
        data: { title: "Info" }
      },
      {
        name: "quiz",
        pattern: "quiz"
      }
    ];
  }

  constructor() {
    super();
    this.route = "";
    this.params = {};
    this.query = {};
    this.data = {};
  }

  router(route, params, query, data) {
    this.route = route;
    this.params = params;
    this.query = query;
    this.data = data;
    console.log(route, params, query, data);
  }

  render() {
    return html`
      <section id="page-container">
        <header>
          <nav>
            <ul id="menu">
              <app-link href="/">Home</app-link>
              <app-link href="/info">Historia</app-link>
              <app-link href="/quiz">Examen</app-link>
            </ul>
          </nav>
        </header>
        
        <article class="content">
          <app-main active-route=${this.route}>
            <home-component route="home"></home-component>
            <historia-component route="info"></historia-component>
            <app-quiz route="quiz"></app-quiz>
          </app-main>
        </article>
        
        <footer>
          <section>
            <p>Equipo 6 - Curso Practitioner Marzo - Abril 2021</p>
          </section>
        </footer>
      </section>
    `;
  }
}

customElements.define('agile-test-simulator', App);