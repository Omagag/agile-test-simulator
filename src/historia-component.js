import { LitElement, html, css } from 'lit-element';

class HistoriaComponent  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
        width: 100%;
        margin-left: auto;
        margin-right: auto;
        font-family: sans-serif;      
        display: flex;
        justify-content: center;
      }
      article {
        width: 60%;
      }
      h2 {
        color: #072146;
      }
      p {
        color: #444;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <article>
        <title>Un poco de historia BBVA</title>
        <h2>1932</h2>
        <p>Se funda Bancomer en la Ciudad de México, bajo el nombre de Banco de Comercio.</p>
        <h2>1982</h2>
        <p>El gobierno mexicano nacionalizó la banca mexicana, incluyendo a Bancomer.</p>
        <h2>Julio 2000</h2>
        <p>BBVA capitaliza a Bancomer con $1,400'000,000 de dólares y nace Grupo Financiero BBVA Bancomer (GFBB), de la fusión de Grupo Financiero BBV-Probursa con Grupo Financiero Bancomer.</p>
        <h2>Julio 2009</h2>
        <p>Se lleva a cabo la fusión de BBVA Bancomer, S.A., como fusionante y BBVA Bancomer Servicios, S.A. como fusionada., con la autorización de la Comisión Nacional Bancaria y de Valores, así con la aprobación del Banco de México.</p>
        <h2>Enero 2013</h2>
        <p>El 9 de enero de 2013 se concretó la venta de la subsidiaria Afore Bancomer a Afore XXI Banorte, trasladando en esta fecha la administración y control de la sociedad.</p>
      </article>
    `;
  }
}

customElements.define('historia-component', HistoriaComponent);