import { LitElement, html, css } from 'lit-element';

class HomeComponent  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
      div {
        width: 100%;
        height: 800px;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      div img {
        width: 60%;
        vertical-align: middle;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <div>
        <img class="center" src="./img/BBVA_TAGLINE_ESP_RGB RIGHT DDB.png"/>
      </div>
    `;
  }
}

customElements.define('home-component', HomeComponent);